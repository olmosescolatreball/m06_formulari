Nom: L’usuari ha de poder escriure el seu nom. Ha de tenir una longitud de 40, però com a molt ha de poder escriure 30 caràcters.
1er Cognom: L’usuari ha de poder escriure el seu cognom. Ha de tenir una longitud de 80, però com a molt ha de poder escriure 60 caràcters.
2on Cognom: L’usuari ha de poder escriure el seu cognom. Ha de tenir una longitud de 80, però com a molt ha de poder escriure 60 caràcters.
Tipus de document: DNI/NIE
Si es DNI: L’usuari ha de poder escriure el seu número i lletra del DNI. Ha de tenir una longitud de 9, i com a molt ha de poder escriure 8 caràcters i ha de tenir un format vàlid.
Si es NIE: L’usuari ha de poder escriure la lletra seguida de 7 números.
Ets: Home / dona
Data naixement. Format dd/mm/aaaa.
No t’has de poder registrar si ets menor de 18 anys.
Nivell d’estudis. llistat Opcions: Educació secundària obligatòria, Programes de formació i inserció, Batxillerat, Formació professional, Ensenyaments artístics superiors, Ensenyaments esportius, Idiomes a les EOI, Educació d'adults, Cursos per accedir a cicles, Itineraris formatius específics, Estudis estrangers
Situació laboral: estudiant, aturat, treball compte propi, treball compte aliè, pensionista.
Nacionalitat :espanyola, UE, FUE
Pais: Busca un llistat de Països per Internet.
